const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/blog.js', 'public/js')
    .js('resources/assets/js/admin.js', 'public/js');
mix.sass('resources/assets/sass/blog.scss', 'public/css/blog.css')
   .sass('resources/assets/sass/admin.scss', 'public/css/admin.css');
