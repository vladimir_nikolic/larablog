

## About Simple Blog
Simple blog is based on [Laravel](http://laravel.com), Vue.js and Bulma.css
No guarantees are given by using this software however.

## Installation
 Do not forget to run php artisan storage:link for creating a similar link for posts photos

## Contributing

Thank you for considering contributing to the Simple Blog! Feel free to make a pull request

## Security Vulnerabilities

If you discover a security vulnerability within Blog, please send an e-mail to Vladimir Nikolic at admin@vladimirnikolic.info. All security vulnerabilities will be promptly addressed.

## License

The Simple Blog is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
