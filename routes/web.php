<?php

Route::get('/', 'BlogController@index');

// Route::get('/admin', 'AdminController@index')->name('home');
Route::group(['middleware' => 'auth'], function () {
    Route::post('/admin/tasks', 'TaskController@store');
    Route::get('/admin/tasks', 'TaskController@index');
    Route::get('/admin/tasks/complete/{id}', 'TaskController@complete');
    Route::get('/admin/tasks/restore/{id}', 'TaskController@uncomplete');
    Route::get('/admin', 'Admin\DashboardController@index')->name('home');
    Route::get('/admin/post/create', 'Admin\PostController@create');
    Route::post('/admin/post/create', 'Admin\PostController@store');
    Route::get('/admin/posts', 'Admin\PostController@index');
    Route::get('/admin/tags', 'Admin\TagController@index');
    Route::get('/admin/todo', function ()
    {
        return view('admin.tasks');
    });
});
Route::get('/blog/{slug}', 'PostsController@show');
Route::get('/posts', 'PostsController@getPaginatedPosts');

Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');

Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');

// pages
Route::get('/contact', function ()
{

});




