<?php

namespace VBlog\Transformers;

use Illuminate\Support\Str;
use Logaretm\Transformers\Transformer;

class PublicPostTransformer extends Transformer
{

    /**
     * @param $post
     *
     * @return mixed
     */
    public function getTransformation($post)
    {
        return [
            'title'   => $post->title,
            'content' => Str::words($post->body, 20),
            'time'    => $post->created_at->diffForHumans(),
            'author'  => $post->user->first_name,
            'photo' => $post->featured_photo,
        ];
    }
}