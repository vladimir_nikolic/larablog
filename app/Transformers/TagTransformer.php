<?php

namespace VBlog\Transformers;

use Logaretm\Transformers\Transformer;

class TagTransformer extends Transformer
{

    /**
     * @param $tag
     * @return mixed
     */
    public function getTransformation($tag)
    {
        return [
            'id' => $tag->id,
            'name' => $tag->name
        ];
    }
}