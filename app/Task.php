<?php

namespace VBlog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'user_id', 'title', 'description', 'completed' 
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    /**
    * user method
    *
    * @author Vladimir Nikolic<me@vladimirnikolic.info>
    * @return 
    **/
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
