<?php

namespace VBlog\Http\Controllers;

use Illuminate\Http\Request;
use VBlog\User;
use Validator;

/**
 * Class RegistrationController
 *
 * @package VBlog\Http\Controllers
 */
class RegistrationController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
    	return view('sessions.registration');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
    	// validate the form
    	$validator = Validator::make($request->all(), [
                'first_name'            => 'required',
                'last_name'             => 'required',
                'email'                 => 'required|email|unique:users',
                'password'              => 'required|min:6|max:20',
                'password_confirmation' => 'required|same:password'
            ],
            [
                'first_name.required'   => 'First Name is required',
                'last_name.required'    => 'Last Name is required',
                'email.required'        => 'Email is required',
                'email.email'           => 'Email is invalid',
                'password.required'     => 'Password is required',
                'password.min'          => 'Password needs to have at least 6 characters',
                'password.max'          => 'Password maximum length is 20 characters'
            ]
            );
        if ($validator->fails()) {
            return redirect('/register')
                        ->withErrors($validator)
                        ->withInput($request->all());
        }
    	// create and save user
    	$user = User::create([
        'first_name' => $request['first_name'],
        'last_name' => $request['last_name'],
        'email' => $request['email'],
        'password' => bcrypt($request['password']),
        ]);
    	// sign user in
    	auth()->login($user);
    	// redirect
    	return redirect()->home();
    }
}
