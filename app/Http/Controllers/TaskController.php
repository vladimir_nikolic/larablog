<?php

namespace VBlog\Http\Controllers;

use Illuminate\Http\Request;
use VBlog\Task;

/**
 * Class TaskController
 *
 * @package VBlog\Http\Controllers
 */
class TaskController extends Controller
{

    /**
     * @return Task
     */
    public function index()
    {
    	return Task::orderBy('created_at', 'desc')->withTrashed()->get();
    }


    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:tasks|max:255|min:10',
            'description' => 'required|min:10',
            'user_id' => 'required'
        ]);
        $task = [
            'description' => $request->description,
            'title'       => $request->title,
            'user_id'      => $request->user_id
        ];
        $task = Task::create($task);
        return $task;
    }


    /**
     * @param \VBlog\Task $task
     */
    public function update(Task $task)
    {
    	
    }

    /**
     * @param \VBlog\Task $task
     */
    public function destroy(Task $task)
    {
    	
    }

    /**
    * complete method will set task.deleted_at to current time
    *
    * @author Vladimir Nikolic<me@vladimirnikolic.info>
    *
    * @return 
    **/
    public function complete($id)
    {
        $task = Task::find($id);
        $task->delete();        
    }
    /**
    * restore method will set task.deleted_at to NULL
    *
    * @author Vladimir Nikolic<me@vladimirnikolic.info>
    *
    * @return 
    **/
    public function uncomplete($id)
    {
        $task = Task::onlyTrashed()
                ->where('id', $id)
                ->restore();
                   
    }
}
