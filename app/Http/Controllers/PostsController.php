<?php

namespace VBlog\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\View;
use VBlog\Post;

/**
 * Class PostsController
 *
 * @package VBlog\Http\Controllers
 */
class PostsController extends ApiController
{
    const PER_PAGE = 3;

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('posts.index');
    }

    public function show($slug)
    {
        $post = Post::join('users', ['users.id' => 'posts.user_id'])
                    ->select([
                        'users.first_name',
                        'users.last_name',
                        'posts.created_at',
                        'posts.body',
                        'posts.featured_photo as photo',
                        'posts.title',

                    ])->where('posts.slug', '=', $slug)->first();

        foreach ($post as $item)
        {
            $post->author = $post->first_name . ' ' . $post->last_name;
            $post->time   = \Carbon\Carbon::createFromTimeStamp(strtotime($post->created_at))->diffForHumans();
        }

        return view('blog.posts.post', compact('post'));
    }

    /**
     * Return JSON and transform post
     *
     * @return mixed
     */
    public function getPaginatedPosts()
    {
        $posts = Post::join('users', ['users.id' => 'posts.user_id'])
                     ->orderBy('posts.created_at', 'desc')
                     ->period(request(['month', 'year']))
                     ->paginate(self::PER_PAGE, [
                         'users.first_name',
                         'users.last_name',
                         'posts.created_at',
                         'posts.body',
                         'posts.featured_photo as photo',
                         'posts.title',
                         'posts.slug',
                     ]);
        $posts->map(function ($item, $key)
        {
            $item->content = Str::words($item->body, 70);
            $item->author  = $item->first_name . ' ' . $item->last_name;
            $item->time    = \Carbon\Carbon::createFromTimeStamp(strtotime($item->created_at))->diffForHumans();

            return $item;
        });

        return $posts;
    }
}