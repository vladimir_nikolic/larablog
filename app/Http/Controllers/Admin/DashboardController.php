<?php

namespace VBlog\Http\Controllers\Admin;

use Illuminate\Http\Request;
use VBlog\Http\Controllers\Controller;

/**
 * Class DashboardController
 *
 * @package VBlog\Http\Controllers\Admin
 */
class DashboardController extends Controller
{
    //
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
    	return view('admin.dashboard');
    }
}
