<?php

namespace VBlog\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use VBlog\Http\Controllers\Controller;
use VBlog\Post;
use Illuminate\Support\Facades\Validator;
use VBlog\Tag;

/**
 * Class PostController
 *
 * @package VBlog\Http\Controllers\Admin
 */
class PostController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')
                     ->paginate(3);

        return view('admin.posts.index', compact('posts'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.posts.create');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        // validate the form
        $validator = Validator::make($request->all(),
            [
                'description'    => 'required',
                'title'          => 'required',
                'body'           => 'required|min:50',
                'featured_photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ],
            [
                'description.required'    => 'Meta description is required',
                'title.required'          => 'Title is required',
                'body.required'           => 'Post body is required',
                'body.min'                => 'Post must have at least 50 chars',
                'featured_photo.required' => 'Featured photo is required',
            ]);
        if ($validator->fails())
        {
            return redirect('/admin/post/create')->withErrors($validator)->withInput($request->all());
        }

        $photo = $request->file('featured_photo');
        $featured_photos_path = '/img/featured_photos/';
        $thumbnail_photos_path = '/img/thumbnail_images/';
        // i am gonna save it under same name so in view will  just call it from a different folder
        $photo_name = time() . '.' . $photo->getClientOriginalExtension();

        $thumbnail_path = public_path($thumbnail_photos_path);
        $photo_path = public_path($featured_photos_path);


        $thumb_img = Image::make($photo->getRealPath())->resize(150, 150);
        $thumb_img->save($thumbnail_path . $photo_name, 80);


        $featured_photo = Image::make($photo->getRealPath())->resize(640, null, function($constraint){
            $constraint->aspectRatio();
        });
        $featured_photo->save($photo_path . $photo_name, 80);
        $post = [
            'description'    => $request->description,
            'title'          => $request->title,
            'body'           => $request->body,
            'featured_photo' => $featured_photos_path . $photo_name,
        ];

        $post = auth()->user()->publish(new Post($post));
        $tags = explode(',', request('tags_selected'));
        foreach ($tags as $tag)
        {
            $tag = Tag::where('id', $tag)->first();
            $post->tags()->attach($tag);
        }

        // redirect
        return redirect()->home();
    }
}
