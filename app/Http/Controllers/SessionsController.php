<?php

namespace VBlog\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

/**
 * Class SessionsController
 *
 * @package VBlog\Http\Controllers
 */
class SessionsController extends Controller
{
    /**
     * Shows the login form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
    	return view('forms.login');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'email'                 => 'required|email',
            'password'              => 'required'
        ]);

        // validate the form
        $validator = Validator::make($request->all(),
            [
            'email'                 => 'required|email',
            'password'              => 'required'
            ],
            [
                'email.required'        => 'Email is required',
                'email.email'           => 'Email is invalid',
                'password.required'     => 'Password is required'
            ]
        );
        if ($validator->fails()) {
            return  \GuzzleHttp\json_encode(['errors' => $validator->errors()]);
        }

        auth()->attempt(request(['email', 'password']));
    	// redirect to Profile page // create roles
        return redirect('/admin');
    }

    /**
    * Logout
    *
    * @return Redirect
    **/
    public function destroy()
    {
    	auth()->logout();
    	return redirect('/');
    }
}
