<?php

namespace VBlog\Http\Controllers;

use Illuminate\Http\Request;
use VBlog\Post;

class BlogController extends Controller
{
    public function index()
    {
        $archives = Post::selectRaw('
                        year(created_at) year, 
                        monthname(created_at) month, 
                        count(*) approved
                        ')
                        ->groupBy('month', 'year')
                        ->orderByRaw('min(created_at) desc')
                        ->get()
                        ->toArray();

        return view('blog.index', compact('archives'));
    }
}
