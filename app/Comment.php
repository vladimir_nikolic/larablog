<?php

namespace VBlog;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Comment
 *
 * @package App
 */
class Comment extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
    	return $this->belongsTo(Post::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()//$comment->user->name
    {
    	return $this->belongsTo(User::class);
    }

    /**
     * @param $body
     */
    public function addComment($body)
    {
    	$this->comments()->create(compact('body'));
    }
}
