<?php

namespace VBlog;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Post
 *
 * @package App
 */
class Post extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['keywords', 'description', 'tags', 'title', 'body', 'featured_photo', 'user_id'];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($post)
        {
            $post->slug  = str_slug($post->title);
            $latest_slug = static::whereRaw("slug RLIKE '^{$post->slug}(-[0-9]*)?$'")
                                 ->latest('id')
                                 ->value('slug');
            if ($latest_slug)
            {
                $pieces     = explode('-', $latest_slug);
                $number     = intval(end($pieces));
                $post->slug .= '-' . ($number + 1);
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function scopeApproved($query, $val)
    {
        return $query->where('approved', 1)->get();
    }

    /**
     * Filter blog posts
     *
     * @param $query
     * @param $filters
     */
    public function scopePeriod($query, $filters)
    {
        if ($month = $filters['month'])
        {
            $query->whereMonth('posts.created_at', Carbon::parse($month)->month);
        }
        if ($year = $filters['year'])
        {
            $query->whereYear('posts.created_at', Carbon::parse($year)->year);
        }
    }

    public static function archives()
    {
        return static::selectRaw('
                        year(created_at) year, 
                        monthname(created_at) month, 
                        count(*) approved
                        ')
                   ->groupBy('month', 'year')
                   ->orderByRaw('min(created_at) desc')
                   ->get()
                   ->toArray();
    }
}
