<?php

namespace VBlog;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['from_email', 'from_name', 'message'];
}
