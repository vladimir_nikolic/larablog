<nav class="nav">
    <div class="nav-left">
        <a class="nav-item is-brand" href="#">
           <span class="icon icon is-large">
  <i class="fa fa-dashboard"></i>
</span>
        </a>
    </div>

    <div class="nav-center">
        <a class="nav-item" href="#">
      <span class="icon">
        <i class="fa fa-github"></i>
      </span>
        </a>
        <a class="nav-item" href="#">
      <span class="icon">
        <i class="fa fa-twitter"></i>
      </span>
        </a>
    </div>

    <!-- Using a <label /> here -->
    <label class="nav-toggle" for="nav-toggle-state">
        <span></span>           <!-- ^^^^^^^^^^^^^^^^ -->
        <span></span>
        <span></span>
    </label>

    <!-- This checkbox is hidden -->
    <input type="checkbox" id="nav-toggle-state"/>

    <div class="nav-right nav-menu">
        <a class="nav-item" href="#">
            Home
        </a>
        <a class="nav-item" href="#">
            Documentation
        </a>
        <a class="nav-item" href="#">
            Blog
        </a>

        <span class="nav-item">
      <a class="button">
        <span class="icon">
          <i class="fa fa-twitter"></i>
        </span>
        <span>Tweet</span>
      </a>
      <a class="button is-primary" href="#">
        <span class="icon">
          <i class="fa fa-download"></i>
        </span>
        <span>Download</span>
      </a>
    </span>
    </div>
</nav>