<!-- START ASIDE MENU -->
<aside class="menu">
    <p class="menu-label">General</p>
    <ul class="menu-list">
        <li style="display: flex;  align-items: center;">

                <span class="icon is-small">
                    <i class="fa fa-dashboard"></i>
                </span><a >Dashboard</a></li>
        <li style="display: flex;  align-items: center;">
            <span class="icon is-small">
                    <i class="fa fa-users"></i>
                </span>
            <a>Users</a>
        </li>
    </ul>
    <p class="menu-label">Blog</p>
    <ul class="menu-list ">
        <li>
            <a class="is-active">Manage Blog</a>
            <ul>
                <li><a href="/admin/posts">All posts</a></li>
                <li><a href="/admin/post/create">Create new post</a></li>
                <li><a>Add a member</a></li>
            </ul>
        </li>
        <li><a>Messages</a></li>
        <li><a>Messages list</a></li>
        <li><a>Authentication</a></li>
    </ul>
</aside>
<!-- END ASIDE MENU -->