@extends('layouts.admin')
@section('content')
    <h4>All posts</h4>
    <nav class="pagination">

    </nav>

    {{ $posts->links() }}
    <table class="table is-narrow">
        <thead>
            <tr>
                <th>Title</th>
                <th>Body</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Tags</th>
            </tr>
        </thead>
        <tbody>
        @foreach($posts as $post)
        <tr>
            <td>{{ $post->title }}</td>
            <td>{!! $post->body !!}</td>
            <td>{{ $post->title }}</td>
            <td>{{ $post->title }}</td>
            <td>{{ $post->title }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
    {{ $posts->links() }}
@endsection