<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="{{ isset($page_description) ?: isset($site_description) }}">
    <title>{{ isset($page_title) ?: isset($site_title) }}</title>
    <!-- EXTERNAL CSS HERE -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin.css') }}">
    <style src="vue-multiselect/dist/vue-multiselect.min.css"></style>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.10.0/styles/default.min.css">

    <!-- Theme included stylesheets -->

    <!-- END EXTERNAL CSS
    <!-- ADDITIONAL PAGE CSS HERE -->
    @yield('css')
    <!-- END OF ADDITIONAL PAGE CSS HERE -->
    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token(), ]) !!};
    </script>
</head>
<body>
<div id="admin">
    <div class="container">
        @include('admin.partials.nav')
    </div>
    <div class="container">
        <h1 class="title">Menu</h1>
        <h2 class="subtitle">A simple <strong>menu</strong>, for any type of vertical navigation</h2>
        <hr>
        <div class="columns">
            <div class="column is-2">
                @include('admin.partials.aside')
            </div>
            <div class="column">
                @yield('content')
            </div>
        </div>
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.10.0/highlight.min.js"></script>
<script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<!-- EXTERNAL JS HERE -->
<script src="{{ asset('js/admin.js') }}"></script>
<!-- END OF EXTERNAL JS HERE -->
<!-- LOCAL JS HERE -->
@yield('js')
<!-- END OF LOCAL JS HERE -->
<script>hljs.initHighlightingOnLoad();</script>
</body>
</html>