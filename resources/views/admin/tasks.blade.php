@extends('layouts.admin')
@section('content')
    <tasks></tasks>
@endsection
@section('js')
    <script>


    </script>
    <script>
        const route_prefix = "{{ url(config('lfm.prefix')) }}";
    </script>

    <!-- CKEditor init -->
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        CKEDITOR.replace( 'description', {
            height: 100,

            removeButtons: "Smiley,SpecialChar,Iframe,InsertPre,Styles,Format,Font,FontSize," +
            "Save,NewPage,DocProps,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Undo,Redo,Find," +
            "Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField" +
            "Outdent,Indent,CreateDiv,BidiLtr,BidiRtl,CreatePlaceholder,Flash,ShowBlocks,HiddenField,Setlanguage",
            filebrowserImageBrowseUrl: route_prefix + '?type=Images',
            filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
            filebrowserBrowseUrl: route_prefix + '?type=Files',
            filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}',
            toolbar: [
                { name: 'document', items: [ 'Source', '-', 'Image', 'Insert code snippet', '-', 'Templates' ] },
                { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
                '/',
                { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
            ]
        });

       /*  $('#description').ckeditor({
            height: 100,
            filebrowserImageBrowseUrl: route_prefix + '?type=Images',
            filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
            filebrowserBrowseUrl: route_prefix + '?type=Files',
            filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
        });*/

    </script>
    @endsection