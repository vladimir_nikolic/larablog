<footer>
    <div class="columns is-primary is-inverted is-outlined">
        <div class="column is-one-quarter">
            First part of footer
        </div>
        <div class="column is-primary is-inverted is-outlined">
            Some part of footer here
        </div>
    </div>
</footer>