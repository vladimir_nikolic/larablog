<div class="column is-one-quarter">
    <aside class="menu">
        <div class="box">
            <div>
                <figure class="image is-3by3">
                    <img src="{{ asset('img/avatars/vladimir.jpg') }}" alt="Vladimir Nikolic">
                </figure>
                <div>
                    <h4 @click='showLoginForm = true' class='login'>Vladimir Nikolic</h4>
                </div>
            </div>
            <p class="control">
                <em>Full time Daddy and the rest of the time developer.</em>
            </p>
            <div class="content social">
                <a class="button github is-small">
                <span class="icon">
                  <i class="fa fa-github"></i>
                </span>
                </a>
                <a class="button twitter is-small">
                <span class="icon">
                  <i class="fa fa-twitter"></i>
                </span>
                </a>
                <a class="button linkedin is-small">
                    <span class="icon">
                      <i class="fa fa-linkedin"></i>
                    </span>
                </a>
                <a class="button facebook is-small" >
                    <span class="icon facebook">
                      <i class="fa fa-facebook facebook"></i>
                    </span>
                </a>
            </div>
        </div>
        <div class="box">
            <p class="menu-label">archive</p>
            <ul class="menu-list">
                @foreach($archives as $archive)
                <li>
                    <a @click="setFilter([month = '{{ $archive['month'] }}', year='{{ $archive['year'] }}'])">
                        {{ $archive['month'] . ' ' . $archive['year']}} ({{ $archive['approved'] }})
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </aside>
</div>
