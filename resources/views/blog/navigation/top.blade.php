<nav class="nav has-shadow">
    <div class="container">
        <div class="nav-left">
            <a class="nav-item is-brand is-hidden-tablet" href="#">
           <span class="icon icon is-medium">
              <i class="fa fa-code" aria-hidden="true" style="color: orangered;"></i>
           </span>
                <span class="is-warning"><em>Hello World!</em></span>
            </a>
        </div>
        <div class="nav-center">
            <a class="nav-item is-tab is-hidden-mobile is-active" href="/">Home</a>
            <!-- <a class="nav-item is-tab is-hidden-mobile" href="/skills">About</a> -->
            <a class="nav-item is-tab is-hidden-mobile" href="/contact">Contact</a>
        </div>
        <!-- Using a <label /> here -->
        <label class="nav-toggle" for="nav-toggle-state">
            <span></span>           <!-- ^^^^^^^^^^^^^^^^ -->
            <span></span>
            <span></span>
        </label>

        <!-- This checkbox is hidden -->
        <input type="checkbox" id="nav-toggle-state"/>

        <div class="nav-right nav-menu">
            <a class="nav-item is-tab is-hidden-tablet is-active" href="/">Home</a>
            <a class="nav-item is-tab is-hidden-tablet" href="/skills">About</a>
            <a class="nav-item is-tab is-hidden-tablet" href="/contact">Contact</a>
        </div>
    </div>
</nav>