@extends('layouts.master')
@section('content')
    <div class="columns">
        <div class="column">
            <v-blog :filter='{month: this.month, year: this.year}'></v-blog>
        </div>
        <transition name="component-fade" mode="out-in">
            <v-login-modal v-if="showLoginForm" @close='showLoginForm = false' v-on:keyup.esc='showLoginForm=false'></v-login-modal>
        </transition>
        @include('blog.navigation.aside')
    </div>
@endsection
@section('js')
    <!-- Load Facebook SDK for JavaScript -->


@endsection