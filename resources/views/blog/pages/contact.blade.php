@extends('layouts.master')
@section('content')
    <div class="section">
        <div class="container">
            <div class="control is-horizontal">
                <div class="control-label">
                    <label class="label">From</label>
                </div>
                <div class="control is-grouped">
                    <p class="control is-expanded">
                        <input class="input" type="text" placeholder="Name" name="name">
                    </p>
                    <p class="control is-expanded">
                        <input class="input" type="email" placeholder="Email" name="email" required>
                    </p>
                </div>
            </div>
            <div class="control is-horizontal">
                <div class="control-label">
                    <label class="label">Question</label>
                </div>
                <div class="control">
                    <textarea class="textarea" placeholder="Tell me how I can help you ;)" name="message" required></textarea>
                </div>
            </div>
        </div>
    </div>


@endsection
