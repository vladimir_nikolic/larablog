@extends('layouts.master')
@section('content')
    <div class="columns">
        <div class="column">
            <div class='box'>
                <div class="card">
                    @if($post->photo)
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img src="{{ $post->photo }}" alt="Image">
                            </figure>
                        </div>
                    @endif
                    <div>
                        <div class="content social">
                            <a class="button github is-small">
                                <span class="icon">
                                  <i class="fa fa-github"></i>
                                </span>
                            </a>
                            <a class="button twitter is-small">
                                <span class="icon">
                                  <i class="fa fa-twitter"></i>
                                </span>
                            </a>
                            <a class="button linkedin is-small">
                                <span class="icon">
                                  <i class="fa fa-linkedin"></i>
                                </span>
                            </a>
                            <a class="button facebook is-small"
                               href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(('/blog/' . $post->slug)) }}">
                                <span class="icon facebook">
                                  <i class="fa fa-facebook facebook"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="media">
                            <section class="">
                                <div class="container">
                                    <div>
                                        <h1 class='title'>{{ $post->title }}</h1>
                                        <div>
                                            <em>
                                                <strong>
                                                    by {{ $post->author }}<small>{{$post->time}}</small>
                                                </strong>
                                            </em>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="content">{!! $post->body !!}</div>
                    </div>
                </div>
            </div>
        </div>
        @include('blog.navigation.aside')
    </div>
@endsection