<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="vladimirnikolic.info">
    <meta name="description" content="{{ isset($page_description) ?: isset($site_description) }}">
    <title>{{ isset($page_title) ?: isset($site_title) ?: "Vladimir Nikolic Blog"}}</title>
    <!-- EXTERNAL CSS HERE˚∫ -->

    <link rel="stylesheet" type="text/css" href="{{ asset('css/blog.css') }}">
    <!-- END EXTERNAL CSS
    <!-- ADDITIONAL PAGE CSS HERE -->
    @yield('css')
    <!-- END OF ADDITIONAL PAGE CSS HERE -->
    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token(), ]) !!};
    </script>
</head>
<body>
        @include('blog.navigation.top')
        <main class="container">
            <div id="app">
                @yield('content')
            </div>
        </main>
        <!--@include('blog.partials.footer')-->

    <!-- EXTERNAL JS HERE -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.10.0/highlight.min.js"></script>
    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="{{ asset('js/blog.js') }}"></script>
    <!-- END OF EXTERNAL JS HERE -->
    <!-- LOCAL JS HERE -->
    @yield('js')
    <!-- END OF LOCAL JS HERE -->
        <script>hljs.initHighlightingOnLoad();</script>
</body>
</html>