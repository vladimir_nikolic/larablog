@if ($paginator->hasPages())
    <nav class="pagination is-centered">
        <a class="pagination-previous" href="{{ $paginator->previousPageUrl() }}" @if(!$paginator->previousPageUrl()) disabled @endif>Previous</a>
        <a class="pagination-next" href="{{ $paginator->nextPageUrl() }}" @if(!$paginator->nextPageUrl()) disabled @endif>Next page</a>
        <ul class="pagination-list">
            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    {{--<li class="disabled"><span>{{ $element }} ovo </span></li>--}}
                    <li><span class="pagination-ellipsis">&hellip;</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li><a class="pagination-link is-current">{{ $page }}</a></li>
                        @else
                            <li><a class="pagination-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </ul>
    </nav>
@endif
