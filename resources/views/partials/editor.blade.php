<form method="POST" action="/admin/post/create" enctype="multipart/form-data">
    {{ csrf_field() }}
    <label for="title" class="label">Meta description</label>
    <p class="control">
        <input type="text" name='description' id='description' class="input" placeholder="Meta description" value="{{ old('description') }}">
        <span class="help is-info">Meta description to be used in head tag</span>
    </p>
    <label for="title" class="label">Title</label>
    <p class="control">
      <input type="text" class="input" id="title" name='title' placeholder="Post title" value="{{ old('title') }}">
      <span class="help is-info">Title- use as a post title and a page title</span>
    </p>
    <label for="body" class="label">Post content/body</label>
    <p class="control">
        <textarea class="textarea" name="body" id="body" rows="3">
            {{ old('body', 'test editor content') }}
        </textarea>
    </p>
    <label for="title" class="label">Title</label>
    <p class="control">
    <div>
        <label class="typo__label">Simple select / dropdown</label>
        <multiselect v-model="value" @input='updateSelected' :options="options" :multiple="true" :close-on-select="false" :clear-on-select="false" :hide-selected="true" placeholder="Pick tags" label="name" track-by="id"></multiselect>
        <input name="tags_selected" id="tags_selected" value="" type="hidden">
    </div>
        <span class="help is-info">Title- use as a post title and a page title</span>
    </p>
    </p>


    <label for="featured_photo" class="label">Featured photo</label>
    <p class="control">
        <input type="file" class="form-control-file" id="featured_photo" name="featured_photo" >
        <span class="help is-info">Upload a featured photo for the post, if any</span>
    </p>
    <p class="control">
    <div class="control is-grouped is-pulled-right">
        <p class="control">
            <button class="button is-primary">Create post</button>
        </p>
        <p class="control">
            <button class="button is-link">Cancel</button>
        </p>
    </div>
</p>
</form>

@section('js')
    <script>
        var route_prefix = "{{ url(config('lfm.prefix')) }}";
    </script>

    <!-- CKEditor init -->
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('#body').ckeditor({
            height: 100,
            filebrowserImageBrowseUrl: route_prefix + '?type=Images',
            filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
            filebrowserBrowseUrl: route_prefix + '?type=Files',
            filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
        });

    </script>
@endsection