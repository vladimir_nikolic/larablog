
window._ = require('lodash');
window.Vue = require('vue');
window.axios = require('axios');
window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};
function url (str) {
    return str.replace(/\s+/g, '-')
}
Vue.prototype.url = url;

window.Event = new class{
    constructor(){
        this.vue = new Vue();
    }
    fire(event, data = null){
        this.vue.$emit(event,  data);
    }
    listen(event, callback){
        this.vue.$on(event,  callback);
    }
};