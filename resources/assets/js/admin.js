require('./thcoder');

import Multiselect from 'vue-multiselect';
import Tasks from './components/tasks/TaskList.vue'
const app = new Vue({
    el: '#admin',
    components: {
        Multiselect, Tasks
    },
    data: {
        value: null,
        options: [],
        tags_selected: [],

    },
    methods: {
        updateSelected: function ()
        {
            this.value.map((index) =>
            {
                this.tags_selected.push(index.id)
            });
            document.getElementById('tags_selected').value = this.clean(this.tags_selected);
        },
        clean: (arr) =>
        {
            let tmp = [];
            for (let i = 0; i < arr.length; i++)
            {
                if (tmp.indexOf(arr[i]) == -1)
                {
                    tmp.push(arr[i]);
                }
            }
            return tmp;

        }
    },
    mounted(){
        axios.get('/admin/tags')
             .then((response) =>
             {
                 if (response.data.length > 0)
                 {
                     this.options = response.data;
                 }
             })
             .catch(function (error)
             {
                 console.log(error);
             });
    }
});