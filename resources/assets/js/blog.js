require('./thcoder');
import Blog from './components/blog.vue';
import LoginModal from './components/modals/LoginModal.vue';
import Exam from './components/ThaiDriverExam.vue';
const app = new Vue({
    el: '#app',
    data: {
        showLoginForm: false,
        month: '',
        year: ''
    },
    methods: {
        setFilter: (filter) => {
            this.month = filter[0];
            this.year = filter[1];
        }
    },
    components: {
        'v-blog': Blog,
        'v-login-modal': LoginModal,
        'thai-driver-exam' : Exam
    }
});
