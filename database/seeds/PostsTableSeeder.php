<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use VBlog\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1,20) as $index) {
            $title = $faker->words(4, true);
            Post::create([
                'approved' => 1,
                'description' => $faker->sentence(),
                'user_id' => 1,
                'title'=> $title,
                'body'=> $faker->paragraphs(4, true),
                'slug' => $title,
                'featured_photo'=>$faker->imageUrl(),
            ]);
        }
    }
}
