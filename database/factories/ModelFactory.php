<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(VBlog\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'first_name' => $faker->name,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
$factory->define(VBlog\Post::class, function (Faker\Generator $faker) {
    $title = $faker->sentence;
    return [
        'approved' => 1,
        'description' => $faker->sentence(),
        'user_id' => 1,
        'title'=> $title,
        'body'=> $faker->paragraphs(4, true),
        'slug' => $title,
        'featured_photo'=>$faker->imageUrl(),
    ];
});
