<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @test
     *
     * @return void
     */
    public function if_login_form_works_and_return_proper_messages()
    {
        $this->browse(function ($browser)
        {
            $browser->visit('/')
                    ->click('.login')
                    ->assertSee('ADMIN LOGIN')
                    ->assertSee('Email')
                    ->assertSee('Password')
                    ->press('Login')
                    ->waitFor('.help', 1)
                    ->assertSee('The email field is required.')
                    ->type('email', 'bleh')
                    ->press('Login')
                    ->pause(1000)
                    ->waitFor('.help')
                    ->assertSee('The email must be a valid email address.')
                    ->type('email', 'wrong@mail.com')
                    ->type('password', 'wrong password')
                    ->press('Login')
                    ->pause(1000)
                    ->waitFor('.help')
                    ->assertSee('Wrong credentials!');

        });
    }

    /**
     * A Dusk test example.
     *
     * @test
     *
     * @return void
     */
    public function if_login_form_has_all_fields()
    {
        $this->browse(function ($browser)
        {
            $browser->whenAvailable('.modal', function ($modal) {
                $modal->assertSee('Email');
                $modal->assertSee('Password');
            });
        });
    }
}
