<?php

class ProfilingTestListener extends \PHPUnit_Framework_BaseTestListener
{
    public function endTest(\PHPUnit_Framework_Test $test, $time)
    {
        printf("Testing '%s'.\tTime %s s.\n",
            str_pad($test->toString(), 10),
            number_format($time, 3)
        );
    }
}