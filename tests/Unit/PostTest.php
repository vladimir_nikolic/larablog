<?php
namespace Tests\Unit;

use Carbon\Carbon;
use Tests\TestCase;
use VBlog\Post;

class PostTest extends TestCase
{
    /**
     * Testing that post will automatically set title on creation
     *
     * @test
     *
     * @return void
     */
    public function a_post_sets_slug_on_create()
    {
        $post = factory('VBlog\Post')->create(['title' => 'Test Blog Post']);
        $this->assertEquals('test-blog-post', $post->slug);
    }

    /**
     * Testing if slg number will be incremented
     *
     * @test
     *
     * @return void
     */
    public function is_a_post_slug_incremented()
    {
        factory('VBlog\Post')->create(['title' => 'Test Blog Post']); // test-blog-post
        factory('VBlog\Post')->create(['title' => 'Test Blog Post']); // test-blog-post-1
        $latest_post = factory('VBlog\Post')->create(['title' => 'Test Blog Post']); // test-blog-post-2
        $this->assertEquals('test-blog-post-2', $latest_post->slug);
    }

    /**
     * @test
     */
    public function it_reads_archive()
    {
        $first = factory(Post::class)->create();

        $second = factory(Post::class)->create(['created_at' => '2017-04-03']);
        factory(Post::class)->create(['created_at' => '2017-04-02']);
        $posts = Post::archives();

        $this->assertEquals([
                        0 =>  [
                            "year" => (int) $first->created_at->format('Y'),
                                "month" => (string) $first->created_at->format('F'),
                                "approved" => 1,
                            ],
                        1 => [
                            "year" => (int) $second->created_at->format('Y'),
                            "month" => (string) $second->created_at->format('F'),
                            "approved" => 2
            ]

        ], $posts);
    }
}
